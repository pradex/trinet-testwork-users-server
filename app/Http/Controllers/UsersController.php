<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mockery\Exception;

class UsersController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->get('data');
        try{
            $result = User::insert($data);
        }
        catch (\Exception $exception){
            return new JsonResponse([
                'status' => 'fail',
                'data' => $exception->getMessage()
            ]);
        }
        return new JsonResponse([
            'status' => 'success',
            'data' => $result
        ]);
    }

    public function update(Request $request)
    {
        $errors = [];

        foreach ($request->get('data') as $item) {
            try{
                User::where('email', $item['email'])->update($item);
            }
            catch (Exception $exception){
                $errors[] = $exception->getMessage();
            }
        }

        if (count($errors)){
            return new JsonResponse([
                'status' => 'fail',
                'data' => $errors
            ]);
        }

        return new JsonResponse([
            'status' => 'success',
        ]);
    }

    public function index()
    {
        $users = User::orderBy('created_at', 'asc')->paginate(100);
        return view('welcome', ['users' => $users]);
    }

    public function filter(Request $request)
    {

        $q = User::query();

        if ($request->get('updated_from')){
            $q->where('updated_at', '>', $request->get('updated_from'));
        }
        if ($request->get('updated_to')){
            $q->where('updated_at', '<=', $request->get('updated_to'));
        }
        if ($request->get('created_from')){
            $q->where('created_at', '>', $request->get('created_from'));
        }
        if ($request->get('created_to')){
            $q->where('created_at', '>=', $request->get('created_from'));
        }

        return $q->get(['email'])->pluck('email');
    }
}
