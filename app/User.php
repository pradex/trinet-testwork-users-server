<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'email';
    public $incrementing = false;

    protected $fillable = [
        'login',
        'email',
        'created_at',
        'updated_at',
    ];
}
