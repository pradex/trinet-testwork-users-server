<!DOCTYPE html>
<html>
<head>
    <title>Список пользователей (Сервер)</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<div class="container">
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">Login</th>
            <th scope="col">Email</th>
            <th scope="col">Создан</th>
            <th scope="col">Обновлен</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->login }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->created_at }}</td>
            <td>{{ $user->updated_at }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    {{ $users->links() }}
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
